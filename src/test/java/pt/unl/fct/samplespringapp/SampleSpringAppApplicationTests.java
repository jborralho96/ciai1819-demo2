package pt.unl.fct.samplespringapp;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MockMvcBuilder;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import pt.unl.fct.samplespringapp.model.PeopleRepository;
import pt.unl.fct.samplespringapp.model.Person;
import pt.unl.fct.samplespringapp.model.Pet;

import static org.hamcrest.Matchers.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SampleSpringAppApplicationTests {

    @Autowired
    private WebApplicationContext wac;

    @Autowired
    private PeopleRepository people;

    private MockMvc mockMvc;

    @Before
    public void init() {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).build();
        people.save(new Person(0, "John"));
        people.save(new Person(0, "Mary"));
    }

    @Test
    public void testGetRecords() throws Exception {
        this.mockMvc.perform(get("/people"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$",hasSize(2)))
                .andExpect(jsonPath("$[0].name", is("John")))
                .andExpect(jsonPath("$[1].name", is("John")))
        ;
    }


    @Test
    public void addPets() throws Exception {

        this.mockMvc.perform(post("/people/1/pets")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content("{\"id\":\"0\", \"name\":\"bobby\"}"))
                .andExpect(status().isOk());

        this.mockMvc.perform(get("/people/1/pets"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$",hasSize(1)))
                .andExpect(jsonPath("$[1].name", is("bobby")));
    }

    @Test
    public void addRemovePets() throws Exception {

        ObjectMapper mapper = new ObjectMapper();

        Pet pet = new Pet("pio");
        String json = mapper.writeValueAsString(pet);

        this.mockMvc.perform(post("/people/1/pets")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(json))
                .andExpect(status().isOk());

        this.mockMvc.perform(delete("/people/1/pets/."))
                .andExpect(status().isOk());

        this.mockMvc.perform(get("/people/1/pets"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$",hasSize(1)));
    }
}
