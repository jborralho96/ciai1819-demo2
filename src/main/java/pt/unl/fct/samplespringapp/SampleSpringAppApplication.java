package pt.unl.fct.samplespringapp;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import pt.unl.fct.samplespringapp.model.PeopleRepository;
import pt.unl.fct.samplespringapp.model.Person;
import pt.unl.fct.samplespringapp.model.Pet;
import pt.unl.fct.samplespringapp.model.PetRepository;

import javax.transaction.Transactional;

@SpringBootApplication
public class SampleSpringAppApplication implements CommandLineRunner {

    public static void main(String[] args) {

        SpringApplication.run(SampleSpringAppApplication.class, args);
    }

    @Autowired
    PeopleRepository people;

    @Autowired
    PetRepository pets;

    @Override
    @Transactional
    public void run(String... args) throws Exception {
    }
}
