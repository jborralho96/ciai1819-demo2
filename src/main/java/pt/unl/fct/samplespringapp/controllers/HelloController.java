package pt.unl.fct.samplespringapp.controllers;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/hello")
public class HelloController {

    @RequestMapping("")
    String generalGreetings() {
        return "Hello, world!";
    }

    @RequestMapping("/{name}")
    String particularGreeting(@PathVariable String name) {
        return "Hello, "+name;
    }
}
